CC=gcc
CFLAGS=-Wall -g
LIBRARYFLAGS=-pthread

DIR_SRC=src/
DIR_OBJ=build/

_OBJ= main.o entities.o
OBJ = $(patsubst %,$(DIR_OBJ)%,$(_OBJ))

all : clean compile exec

exec :
	@build/./main
	@echo "Terminando a Execução."

$(DIR_OBJ)%.o: $(DIR_SRC)%.c
	@$(CC) $(CFLAGS) -c -o $@ $< $(LIBRARYFLAGS)

compile: $(OBJ)
	@$(CC) $(CFLAGS) $(LIBRARYFLAGS) $^ -o build/main

clean :
	@rm -rf build
	@mkdir build