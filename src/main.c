/*
 * Programação Concorrente
 * Trabalho - Sincronização de Projetos
 * Semana do Churrasco
 * Aluno: Danilo Santos de Sales
 * Matricula: 14/0135910
 */

#include "entities.h"
#include "stdio.h"
#include "stdlib.h"

int main(void) {
    thread_init();
    // Execution on concurrency
    thread_join();
    return EXIT_SUCCESS;
}
