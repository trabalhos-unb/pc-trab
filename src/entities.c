#include "entities.h"

void* person(void* arg) {
    int item;
    while (true) {
        // Verifica se o buffer está vazio
        pthread_mutex_lock(&lock);
        while (should_consumer_sleep()) pthread_cond_wait(&person_cond, &lock);
        pthread_mutex_unlock(&lock);
        // Remove o item
        pthread_mutex_lock(&lock);
        item = remove_item();
        pthread_mutex_unlock(&lock);
        // Acorda os produtores que dormem
        if (should_producer_wakeup()) pthread_cond_signal(&person_cond);
        // Consome o item
        printf("Item %d\n", item);
    }
    pthread_exit(EXIT_SUCCESS);
}

void* steakhouse(void* arg) {
    int item;
    while (true) {
        // Verifica se o buffer está vazio
        pthread_mutex_lock(&lock);
        while (should_consumer_sleep())
            pthread_cond_wait(&steakhouse_cond, &lock);
        pthread_mutex_unlock(&lock);
        // Remove o item
        pthread_mutex_lock(&lock);
        item = remove_item();
        pthread_mutex_unlock(&lock);
        // Acorda os produtores que dormem
        if (should_producer_wakeup()) pthread_cond_signal(&steakhouse_cond);
        // Consome o item
        printf("Item %d\n", item);
    }
    pthread_exit(EXIT_SUCCESS);
}

void* bucher(void* arg) {
    int item;
    while (true) {
        item = produce_item();
        // Verifica se o beef está cheio
        pthread_mutex_lock(&lock);
        if (should_bucher_sleep()) pthread_cond_wait(&bucher_cond, &lock);
        pthread_mutex_unlock(&lock);
        // Inseri os itens no beef
        pthread_mutex_lock(&lock);
        insert_item(item);
        pthread_mutex_unlock(&lock);
        // Acorda os consumidores que dormem
        if (should_person_wakeup()) pthread_cond_signal(&person_cond);
        if (should_steakhouse_wakeup()) pthread_cond_signal(&bucher_cond);
    }
    pthread_exit(EXIT_SUCCESS);
}

void thread_init() {
    for (int i = 0; i < N_PERSON; i++) {
        pthread_create(&threads[i], NULL, person, NULL);
    }
    for (int i = N_PERSON; i < N_PERSON + N_STEAKHOUSE; i++) {
        pthread_create(&threads[i], NULL, steakhouse, NULL);
    }
    for (int i = N_PERSON + N_STEAKHOUSE;
         i < N_PERSON + N_STEAKHOUSE + N_BUCHER; i++) {
        pthread_create(&threads[i], NULL, bucher, NULL);
    }
}

void thread_join() {
    for (int i = 0; i < N_PERSON + N_STEAKHOUSE + N_BUCHER; i++) {
        pthread_join(threads[i], NULL);
    }
}

int produce_item() { return rand() % 10; }

void insert_item(int item) {
    beef[c_beef] = item;
    c_beef = c_beef + 1;
}
int remove_item() {
    int item = beef[c_beef];
    c_beef = c_beef - 1;
    return item;
}

bool should_bucher_wakeup() { return c_beef == (N_BEEF / 2); }
bool should_bucher_sleep() { return c_beef == N_BEEF - 1; }

bool should_person_sleep() { return c_beef == HUNGER_PERSON - 1; }
bool should_steakhouse_sleep() { return c_beef < HUNGER_STEAKHOUSE; }
bool should_person_wakeup() { return c_beef > HUNGER_STEAKHOUSE - 1; }
bool should_steakhouse_wakeup() { return c_beef > HUNGER_STEAKHOUSE - 1; }