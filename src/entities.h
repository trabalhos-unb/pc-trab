#ifndef _ENTITIES_H_
#define _ENTITIES_H_

#include "pthread.h"
#include "stdbool.h"
#include "stdlib.h"

#define N_PERSON 20
#define N_STEAKHOUSE 5
#define N_BUCHER 5
#define N_BEEF 100

#define HUNGER_STEAKHOUSE 10
#define HUNGER_PERSON 1

pthread_t threads[N_PERSON + N_STEAKHOUSE];
bool steaks[N_BEEF];

int c_beef = 0;
int beef[N_BEEF];
int buffer_index = 0;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t insert_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t remove_lock = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t bucher_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t person_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t steakhouse_cond = PTHREAD_COND_INITIALIZER;

void* person(void*);
void* steakhouse(void*);
void* bucher(void*);
void thread_init();
void thread_join();

bool should_person_sleep();
bool should_steakhouse_sleep();
bool should_person_wakeup();
bool should_steakhouse_wakeup();

bool should_bucher_wakeup();
bool should_bucher_sleep();

int produce_item();
void insert_item(int);
int remove_item();

#endif